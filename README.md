<h1>A simple Time Tracker</h1>

Add your projects or customers to the textfile named "project.lst" in the same directory
One Name per line.

The tracker will add the times to a file named "timeslog.csv" which you can easily process with LibreOffice Calc or MS Excel.

No setup procedure necessary. Just download the .exe from the "Release" directory and the "project.lst" and "timeslog.csv"

For the Linux edition you need to have Mono installed, which is available in most distribution's repositories.