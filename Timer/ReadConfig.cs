﻿using System.Collections;
using System.IO;
using System.Windows.Forms;

namespace Timer
{
    static class ReadConfig
    {
        // create an ArrayList for the projects in the config file
        internal static ArrayList projectList = new ArrayList();

        /// <summary>
        /// Retrieve data from config file
        /// </summary>
        /// <param name="configfile"></param>
        /// <returns></returns>
        public static void GetData(string configfile)
        {
            if (!File.Exists(configfile))
            {
                MessageBox.Show("Datei " + configfile + " nicht gefunden.\nProgramm wird beendet.", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Environment.Exit(1);
            }
            else
            {
                FileStream fs = new FileStream(configfile, FileMode.Open);
                StreamReader sr = new StreamReader(fs);
                
                while (sr.Peek() != -1)
                {
                    // store each line of the file to ArrayList
                    projectList.Add(sr.ReadLine());
                }
                sr.Close();
            }

        }
    }
}