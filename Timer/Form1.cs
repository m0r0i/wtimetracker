﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Timer
{
    public partial class Form1 : Form
    {
        Stopwatch sw = new Stopwatch();
        private string sessionStartDate = null;         // session start date
        private string sessionStartTime = null;         // session start time
        private string sessionTime = null;              // session last that long
        private string sessionEndDate = null;           // session end date
        private string sessionEndTime = null;           // session ends at this time

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // visual setting for startup
            SessionTimer.Enabled = false;
            CmdStop.Enabled = false;
            LblSessionStartTime.Text = "";
            LblSessionEndTime.Text = "";
            LblSessionTimer.Text = "";
            

            // Set Information to status bar:
            LblStatDate.Text = DateTime.Today.ToLongDateString();
            LblStatUser.Text = "          User: " + Config.UserName;
            LblStatVersion.Text = "     Version " + Config.Version;

            // read the config file and fill the dropdown-box
            ReadConfig.GetData(Config.CstListFileName);
            foreach (var item in ReadConfig.projectList)
            {
                CmbCustomer.Items.Add(item);
            }

           
            // check if a customer/project is selected, only then enable the start button
            if (CmbCustomer.SelectedIndex == -1)
                CmdStart.Enabled = false;
        }


        // The start button for time logging does the following when pressed:
        private void CmdStart_Click(object sender, EventArgs e)
        {
            sw.Start();
            LblCustomerNameHeadline.Enabled = false;                    // disable headline
            CmbCustomer.Enabled = false;                                // disable customer/project selection
            CmdStart.Enabled = false;                                   // disable the start button as it has already been pressed
            CmdStop.Enabled = true;                                     // enable the stop button

			LblSessionEndTime.Text = "";                                // clear the session end time string
			LblSessionTimer.Text = "00:00:00";

            sessionStartDate = DateTime.Now.ToShortDateString();        // this stores the start date of the session
            sessionStartTime = DateTime.Now.ToLongTimeString();         // we set the start time for this session to now
            SessionTimer.Enabled = true;                                // let the timer run!
            
            LblSessionStartTime.Text = "Session start: " + sessionStartDate + " " + sessionStartTime;   // display the start d/t in the window

        }

        // The stop button for time logging does the following when pressed:
        private void CmdStop_Click(object sender, EventArgs e)
        {
            sw.Stop();
            CmdStop.Enabled = false;                                    // disable the stop button
            CmdStart.Enabled = true;                                    // enable the start button

            SessionTimer.Enabled = false;                               // disable the session timer

            sessionEndDate = DateTime.Now.ToShortDateString();
            sessionEndTime = DateTime.Now.ToLongTimeString();           // record the end time of the session

            LblSessionEndTime.Text = "Sesson end : " + sessionEndDate + " " + sessionEndTime;          // display the end d/t in the window
            sw.Reset();

            // write the name + dates + times to .csv file
            try
            {
                WriteLog.ToFile(Config.LogFileName,
                CmbCustomer.SelectedItem.ToString(),
                Config.UserName,
                sessionTime,
                sessionStartDate,
                sessionStartTime,
                sessionEndDate,
                sessionEndTime);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                        

            // re-enable the customer/project selection box and the headline
            CmbCustomer.Enabled = true;
            LblCustomerNameHeadline.Enabled = true;

        }



        private void SessionTimer_Tick(object sender, EventArgs e)
        {
            sessionTime = sw.Elapsed.ToString("hh\\:mm\\:ss");
            LblSessionTimer.Text = sessionTime;

        }


        // what to do if another customer/project is being selected in the dropdown-list:
        private void CmbCustumer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CmbCustomer.SelectedIndex != -1)    // if there's something in the list...
                CmdStart.Enabled = true;            //    ... enable the start button

            LblSessionTimer.Text = "00:00:00";      // reset timer text to zero
            LblSessionStartTime.Text = "";          // clear start time label
            LblSessionEndTime.Text = "";            // clear end time label
        }

    }
}
