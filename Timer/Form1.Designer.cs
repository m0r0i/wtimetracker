﻿namespace Timer
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.CmbCustomer = new System.Windows.Forms.ComboBox();
            this.CmdStart = new System.Windows.Forms.Button();
            this.CmdStop = new System.Windows.Forms.Button();
            this.LblSessionStartTime = new System.Windows.Forms.Label();
            this.LblCustomerNameHeadline = new System.Windows.Forms.Label();
            this.LblSessionTimeHeadline = new System.Windows.Forms.Label();
            this.SessionTimer = new System.Windows.Forms.Timer(this.components);
            this.LblSessionEndTime = new System.Windows.Forms.Label();
            this.LblSessionTimer = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblStatDate = new System.Windows.Forms.ToolStripStatusLabel();
            this.LblStatUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.LblStatVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CmbCustomer
            // 
            this.CmbCustomer.FormattingEnabled = true;
            this.CmbCustomer.Location = new System.Drawing.Point(16, 28);
            this.CmbCustomer.Margin = new System.Windows.Forms.Padding(4);
            this.CmbCustomer.Name = "CmbCustomer";
            this.CmbCustomer.Size = new System.Drawing.Size(160, 24);
            this.CmbCustomer.Sorted = true;
            this.CmbCustomer.TabIndex = 0;
            this.CmbCustomer.SelectedIndexChanged += new System.EventHandler(this.CmbCustumer_SelectedIndexChanged);
            // 
            // CmdStart
            // 
            this.CmdStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdStart.Location = new System.Drawing.Point(460, 10);
            this.CmdStart.Margin = new System.Windows.Forms.Padding(4);
            this.CmdStart.Name = "CmdStart";
            this.CmdStart.Size = new System.Drawing.Size(100, 49);
            this.CmdStart.TabIndex = 1;
            this.CmdStart.Text = "S&tart";
            this.CmdStart.UseVisualStyleBackColor = true;
            this.CmdStart.Click += new System.EventHandler(this.CmdStart_Click);
            // 
            // CmdStop
            // 
            this.CmdStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdStop.Location = new System.Drawing.Point(567, 10);
            this.CmdStop.Margin = new System.Windows.Forms.Padding(4);
            this.CmdStop.Name = "CmdStop";
            this.CmdStop.Size = new System.Drawing.Size(100, 49);
            this.CmdStop.TabIndex = 2;
            this.CmdStop.Text = "Sto&p";
            this.CmdStop.UseVisualStyleBackColor = true;
            this.CmdStop.Click += new System.EventHandler(this.CmdStop_Click);
            // 
            // LblSessionStartTime
            // 
            this.LblSessionStartTime.AutoSize = true;
            this.LblSessionStartTime.Location = new System.Drawing.Point(207, 31);
            this.LblSessionStartTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblSessionStartTime.Name = "LblSessionStartTime";
            this.LblSessionStartTime.Size = new System.Drawing.Size(69, 17);
            this.LblSessionStartTime.TabIndex = 3;
            this.LblSessionStartTime.Text = "SessStart";
            // 
            // LblCustomerNameHeadline
            // 
            this.LblCustomerNameHeadline.AutoSize = true;
            this.LblCustomerNameHeadline.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCustomerNameHeadline.Location = new System.Drawing.Point(16, 9);
            this.LblCustomerNameHeadline.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblCustomerNameHeadline.Name = "LblCustomerNameHeadline";
            this.LblCustomerNameHeadline.Size = new System.Drawing.Size(64, 17);
            this.LblCustomerNameHeadline.TabIndex = 5;
            this.LblCustomerNameHeadline.Text = "Project:";
            // 
            // LblSessionTimeHeadline
            // 
            this.LblSessionTimeHeadline.AutoSize = true;
            this.LblSessionTimeHeadline.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSessionTimeHeadline.Location = new System.Drawing.Point(207, 9);
            this.LblSessionTimeHeadline.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblSessionTimeHeadline.Name = "LblSessionTimeHeadline";
            this.LblSessionTimeHeadline.Size = new System.Drawing.Size(105, 17);
            this.LblSessionTimeHeadline.TabIndex = 6;
            this.LblSessionTimeHeadline.Text = "Session time:";
            // 
            // SessionTimer
            // 
            this.SessionTimer.Interval = 1000;
            this.SessionTimer.Tick += new System.EventHandler(this.SessionTimer_Tick);
            // 
            // LblSessionEndTime
            // 
            this.LblSessionEndTime.AutoSize = true;
            this.LblSessionEndTime.Location = new System.Drawing.Point(208, 53);
            this.LblSessionEndTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblSessionEndTime.Name = "LblSessionEndTime";
            this.LblSessionEndTime.Size = new System.Drawing.Size(64, 17);
            this.LblSessionEndTime.TabIndex = 9;
            this.LblSessionEndTime.Text = "SessEnd";
            // 
            // LblSessionTimer
            // 
            this.LblSessionTimer.AutoSize = true;
            this.LblSessionTimer.Location = new System.Drawing.Point(324, 10);
            this.LblSessionTimer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblSessionTimer.Name = "LblSessionTimer";
            this.LblSessionTimer.Size = new System.Drawing.Size(44, 17);
            this.LblSessionTimer.TabIndex = 10;
            this.LblSessionTimer.Text = "Timer";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblStatDate,
            this.LblStatVersion,
            this.LblStatUser});
            this.statusStrip1.Location = new System.Drawing.Point(0, 72);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(683, 26);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LblStatDate
            // 
            this.LblStatDate.Name = "LblStatDate";
            this.LblStatDate.Size = new System.Drawing.Size(41, 20);
            this.LblStatDate.Text = "Date";
            // 
            // LblStatUser
            // 
            this.LblStatUser.Name = "LblStatUser";
            this.LblStatUser.Size = new System.Drawing.Size(38, 20);
            this.LblStatUser.Text = "User";
            // 
            // LblStatVersion
            // 
            this.LblStatVersion.Name = "LblStatVersion";
            this.LblStatVersion.Size = new System.Drawing.Size(30, 20);
            this.LblStatVersion.Text = "Ver";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 98);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.LblSessionTimer);
            this.Controls.Add(this.LblSessionEndTime);
            this.Controls.Add(this.LblSessionTimeHeadline);
            this.Controls.Add(this.LblCustomerNameHeadline);
            this.Controls.Add(this.LblSessionStartTime);
            this.Controls.Add(this.CmdStop);
            this.Controls.Add(this.CmdStart);
            this.Controls.Add(this.CmbCustomer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "wTimeTracker";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button CmdStart;
        private System.Windows.Forms.Button CmdStop;
        private System.Windows.Forms.Label LblSessionStartTime;
        private System.Windows.Forms.Label LblCustomerNameHeadline;
        private System.Windows.Forms.Label LblSessionTimeHeadline;
        private System.Windows.Forms.Timer SessionTimer;
        private System.Windows.Forms.Label LblSessionEndTime;
        private System.Windows.Forms.Label LblSessionTimer;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LblStatDate;
        private System.Windows.Forms.ToolStripStatusLabel LblStatUser;
        private System.Windows.Forms.ToolStripStatusLabel LblStatVersion;
        public System.Windows.Forms.ComboBox CmbCustomer;
    }
}

