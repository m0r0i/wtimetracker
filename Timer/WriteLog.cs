﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Timer
{
    
    static class WriteLog
    {
        static FileStream fs;
        static StreamWriter sw;

        private static string ausgabeZeile;
        /// <summary>
        /// Write the collected data to a csv file
        /// </summary>
        /// <param name="logfilename">The log file name</param>
        /// <param name="cstproject">The project or customer name</param>
        /// <param name="userName">The Windows system user name</param>
        /// <param name="sessionTime">Elapsed session time</param>
        /// <param name="sessionStartDate"></param>
        /// <param name="sessionStartTime"></param>
        /// <param name="sessionEndDate"></param>
        /// <param name="sessionEndTime"></param>
        internal static void ToFile(string logfilename,
                                    string cstproject,
                                    string userName,
                                    string sessionTime,
                                    string sessionStartDate,
                                    string sessionStartTime,
                                    string sessionEndDate,
                                    string sessionEndTime)
        {
            
            try
            {
                fs = new FileStream(logfilename, FileMode.Append);
                sw = new StreamWriter(fs);

                ausgabeZeile = cstproject + "," + userName + "," + sessionTime + "," + sessionStartDate + "," + sessionStartTime + "," + sessionEndDate + "," + sessionEndTime + "\r";
                //MessageBox.Show(AusgabeZeile, "Debug", MessageBoxButtons.OK, MessageBoxIcon.Information);

                sw.Write(ausgabeZeile);
                //sw.WriteLine(ausgabeZeile);     // makes ugly empty lines on Windows :)
                sw.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
