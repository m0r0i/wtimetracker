﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timer
{
    internal static class Config
    {
        public static string CstListFileName = "project.lst";
        public static string LogFileName = "timeslog.csv";
        public static string UserName = Environment.UserName;
        public static string Version = System.Windows.Forms.Application.ProductVersion.ToString();
    }
}
